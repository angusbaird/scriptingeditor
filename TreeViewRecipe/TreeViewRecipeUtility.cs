﻿using Microsoft.Win32;
using OptekMain.GenericClasses;
using Prism.Commands;
using ScriptingDevelopment.Instructions;
using ScriptingDevelopment.Instructions.BaseInstructions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace ScriptingDevelopment.TreeViewRecipe
{
    public class TreeViewRecipeUtility : ViewModel
    {
        private CancellationTokenSource _cts = new CancellationTokenSource();


        private ObservableCollection<Instruction> _topNodes = new ObservableCollection<Instruction>();
        public ObservableCollection<Instruction> TopNodes
        {
            get { return _topNodes; }
            set
            {
                _topNodes = value;
                NotifyPropertyChanged();
            }
        }

        TreeView _treeView;
        public TreeView TreeView
        {
            get { return _treeView; }
            set 
            {
                _treeView = value;
                SelectedInstruction = value.SelectedItem as Instruction;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(SelectedInstruction));
            }
        }

        private ObservableCollection<Instruction> _selectedGeneration = new ObservableCollection<Instruction>();
        public ObservableCollection<Instruction> SelectedGeneration
        {
            get
            {
                return _selectedGeneration;
            }
            set
            {
                _selectedGeneration = value;
                NotifyPropertyChanged();
            }
        }

        Instruction _selectedInstruction;
        public Instruction SelectedInstruction
        {
            get { return _selectedInstruction; }
            set
            {
                _selectedInstruction = value;
                NotifyPropertyChanged();
            }
        }

        private Instruction _clipboard;
        public Instruction Clipboard
        {
            get { return _clipboard; }
            set
            {
                _clipboard = value;
                NotifyPropertyChanged();
            }
        }

        private bool _isScriptRunning;
        public bool IsScriptRunning
        {
            get { return _isScriptRunning; }
            set
            {
                _isScriptRunning = value;
                NotifyPropertyChanged();
            }
        }


        private string _elapsedProgramTime = "00:00:00";
        public string ElapsedProgramTime
        {
            get { return _elapsedProgramTime; }
            set 
            { 
                _elapsedProgramTime = value;
                NotifyPropertyChanged();
            }
        }

        private int _totalSeconds = 0;
        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            _totalSeconds += 1;
            ElapsedProgramTime = string.Format("{0:hh\\:mm\\:ss}", TimeSpan.FromSeconds(this._totalSeconds).Duration());
        }


        public DelegateCommand AddMainMethodBlockCommand { get; set; }
        public DelegateCommand AddForInstructionCommand { get; set; }
        public DelegateCommand AddDelayInstructionCommand { get; set; }
        public DelegateCommand EndCurrentBlockCommand { get; set; }
        public DelegateCommand RemoveInstructionCommand { get; set; }
        
        public DelegateCommand NewScriptCommand { get; set; }
        public DelegateCommand SerialiseCommand { get; set; }
        public DelegateCommand DeserialiseCommand { get; set; }

        public DelegateCommand RunCommand { get; set; }
        public DelegateCommand AbortCommand { get; set; }

        public DelegateCommand CutInstructionCommand { get; set; }
        public DelegateCommand CopyInstructionCommand { get; set; }
        public DelegateCommand PasteInstructionCommand { get; set; }
        public DelegateCommand DeleteInstructionCommand { get; set; }
        public DelegateCommand ToggleCommentOnInstructionCommand { get; set; }



        public TreeViewRecipeUtility()
        {
            AddMainMethodBlockCommand = new DelegateCommand(() => AddMainMethodBlock());
            AddForInstructionCommand = new DelegateCommand(() => AddForInstruction());
            AddDelayInstructionCommand = new DelegateCommand(() => AddDelayInstruction());
            RemoveInstructionCommand = new DelegateCommand(() => RemoveInstruction(SelectedInstruction));

            NewScriptCommand = new DelegateCommand(() => NewScript());
            SerialiseCommand = new DelegateCommand(() => Serialise());
            DeserialiseCommand = new DelegateCommand(() => Deserialise(), () => !IsScriptRunning);
            DeserialiseCommand.ObservesProperty(() => IsScriptRunning);

            RunCommand = new DelegateCommand(async () => await RunProcess(_cts.Token), () => !IsScriptRunning);
            RunCommand.ObservesProperty(() => IsScriptRunning);
            AbortCommand = new DelegateCommand(() => Abort(), () => IsScriptRunning);
            AbortCommand.ObservesProperty(() => IsScriptRunning);

            CutInstructionCommand = new DelegateCommand(CutInstruction, () => SelectedInstruction != null);
            CutInstructionCommand.ObservesProperty(() => SelectedInstruction);
            CopyInstructionCommand = new DelegateCommand(CopyInstruction, () => SelectedInstruction != null);
            CopyInstructionCommand.ObservesProperty(() => SelectedInstruction);
            PasteInstructionCommand = new DelegateCommand(PasteInstruction, () => Clipboard != null);
            PasteInstructionCommand.ObservesProperty(() => Clipboard);
            DeleteInstructionCommand = new DelegateCommand(DeleteInstruction, () => SelectedInstruction != null);
            DeleteInstructionCommand.ObservesProperty(() => SelectedInstruction);
            ToggleCommentOnInstructionCommand = new DelegateCommand(ToggleCommentOnInstruction, () => SelectedInstruction != null);
            ToggleCommentOnInstructionCommand.ObservesProperty(() => SelectedInstruction);

            NewScript();
        }


        private void CutInstruction()
        {
            Clipboard = SelectedInstruction;
            RemoveInstruction(SelectedInstruction);
        }

        private void CopyInstruction()
        {
            Clipboard = SelectedInstruction;
        }

        private void PasteInstruction()
        {
            Instruction shallowCloneOfClipboard = Clipboard.Clone(); // Deep copy: For block copy and paste
            AddInstruction(shallowCloneOfClipboard);
        }

        private void DeleteInstruction()
        {
            RemoveInstruction(SelectedInstruction);
        }

        private void ToggleCommentOnInstruction()
        {
            if (!SelectedInstruction.IsCommentedOut)
                SelectedInstruction.IsCommentedOut = true;
            else
                SelectedInstruction.IsCommentedOut = false;
        }


        public void AddForInstruction()
        {
            ForInstruction For = new ForInstruction();
            AddInstruction(For);
        }

        public void AddDelayInstruction()
        {
            DelayInstruction Delay = new DelayInstruction();
            AddInstruction(Delay);
        }

        public void AddMainMethodBlock()
        {
            MainMethodBlock Main = new MainMethodBlock();
            AddInstruction(Main);
        }


        public void RemoveInstruction(Instruction selectedInstruction)
        {
            RemoveInstructionRecursion(selectedInstruction, TopNodes);

        }
        private void RemoveInstructionRecursion(Instruction selectedInstruction, ObservableCollection<Instruction> generation) //make selected generation automatically update when selected instruction updates from the view, using getgeneraitonfromID as a trigger from propertychanged with selected instruction parent id as the target
        {
            if (selectedInstruction is MainMethodBlock)
                return;

            foreach (Instruction instruction in generation)
            {
                if (generation.Contains(selectedInstruction))
                {
                    generation.Remove(selectedInstruction);
                    break; //Endpoint: after adding to end, or inserting at a specific position
                }
                else if (instruction is BlockInstruction)
                {
                    RemoveInstructionRecursion(selectedInstruction, ((BlockInstruction)instruction).Children);
                }
            }
        }

        public void AddInstruction(Instruction instructionToAdd)
        {
            if (instructionToAdd is MainMethodBlock) // For the main method
            {
                SelectedGeneration.Add(instructionToAdd);
                return;
            }

            AddInstructionRecursion(SelectedInstruction, instructionToAdd, TopNodes);
        }

        public void AddInstructionRecursion(Instruction selectedInstruction, Instruction instructionToAdd, ObservableCollection<Instruction> generation)
        {
            bool isSelectedInstructionABlockInstruction = selectedInstruction is BlockInstruction;

            foreach (Instruction instruction in generation)
            {
                if (generation.Contains(selectedInstruction))
                {
                    int indexOfSelectedInstruction = generation.IndexOf(selectedInstruction);

                    if (isSelectedInstructionABlockInstruction)
                        ((BlockInstruction)selectedInstruction).Children.Add(instructionToAdd);
                    else
                        generation.Insert(indexOfSelectedInstruction + 1, instructionToAdd);
                    break; //Endpoint: after adding new instruction to end, or inserting at a specific position
                }
                else if (instruction is BlockInstruction)
                {
                    AddInstructionRecursion(selectedInstruction, instructionToAdd, ((BlockInstruction)instruction).Children);
                } 
            }
        }

        public ObservableCollection<Instruction> GetBlockInstructionChildren(BlockInstruction blockInstruction, ObservableCollection<Instruction> generation)
        {
            if (generation.Contains(blockInstruction))
            {
                return blockInstruction.Children; //Endpoint
            }
            else 
            {
                foreach (Instruction instruction in generation)
                {
                    if (instruction is BlockInstruction)
                        GetBlockInstructionChildren(blockInstruction, ((BlockInstruction)instruction).Children);
                }
            }
            return blockInstruction.Children; //Endpoint
        }







        public async Task<bool> RunProcess(CancellationToken ct, bool isVirtual = false)
        {
            IsScriptRunning = true;

            _totalSeconds = 0;
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(DispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();

            try
            {
                return await RunProcessTask(TopNodes[0], TopNodes, ct, isVirtual); // TopNodes[0] is the main method at the top level
            }
            catch (TaskCanceledException tcex)
            {
                DispatcherMessagebox.OKMessageBox("The process script was cancelled by the user.", "Process Cancelled");
                return false;
            }
            catch (Exception ex)
            {
                DispatcherMessagebox.OKMessageBox(ex.Message, "Process Exception");
                return false;
            }
            finally
            {
                dispatcherTimer.Stop();
                IsScriptRunning = false;
            }

        }

        private async Task<bool> RunProcessTask(Instruction instructionToExecute, ObservableCollection<Instruction> generation, CancellationToken ct, bool isVirtual = false)
        { 
            if (instructionToExecute is MainMethodBlock)
                return await RunProcessTask(((MainMethodBlock)instructionToExecute).Children[0], ((MainMethodBlock)instructionToExecute).Children, ct, isVirtual);

         
            if (instructionToExecute is ForInstruction)
            {
                await ((ForInstruction)instructionToExecute).Execute(ct); // put execution flags in the for instruction
                ((ForInstruction)instructionToExecute).IsExecuting = true;


                ObservableCollection<Instruction> childInstructions = GetBlockInstructionChildren((ForInstruction)instructionToExecute, TopNodes);                         
                while (((ForInstruction)instructionToExecute).CurrentIteration < ((ForInstruction)instructionToExecute).Iterations)
                {
                    await RunProcessTask(childInstructions[0], childInstructions, ct, isVirtual); // run from the the first instruction in the child generation
                    ((ForInstruction)instructionToExecute).CurrentIteration++;
                }
                ((ForInstruction)instructionToExecute).CurrentIteration = 0;
                ((ForInstruction)instructionToExecute).IsExecuting = false;
            }
                
            if (!await instructionToExecute.Execute(ct, isVirtual)) // single instruction
            {
                return false;
            }

            int nextInstructionIndex = generation.IndexOf(instructionToExecute) + 1;

            if (nextInstructionIndex > generation.Count - 1)
                return true; // end of script reached

            Instruction nextInstruction = generation[nextInstructionIndex];
            await RunProcessTask(nextInstruction, generation, ct, isVirtual);

            return true;
        }


        public void Abort()
        {
            _cts.Cancel();
            _cts = new CancellationTokenSource();
        }














        public void NewScript()
        {
            TopNodes.Clear();
            SelectedGeneration = TopNodes;
            AddMainMethodBlock();
        }



        public void Serialise()
        {
            try
            {
                string savePath = "";

                var saveFileDialog = new SaveFileDialog
                {
                    Filter = "xml (*.xml)|*.xml|rcp (*.rcp)|*.rcp",
                    DefaultExt = "xml",
                    AddExtension = true
                };

                if (saveFileDialog.ShowDialog() == true)
                {
                    if (saveFileDialog.FileName.ToLower().EndsWith(".xml") || saveFileDialog.FileName.ToLower().EndsWith(".rcp"))
                    {
                        savePath = saveFileDialog.FileName;
                        XmlSerialiser.SerialiseToXML<ObservableCollection<Instruction>>(TopNodes, savePath);
                    }
                    else
                    {
                        MessageBox.Show("File Type Needed: .xml or .rcp", "Unacceptable File Type");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Deserialise()
        {
            try
            {
                string savePath = "";

                var openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    if (openFileDialog.FileName.ToLower().EndsWith(".xml") || openFileDialog.FileName.ToLower().EndsWith(".rcp"))
                    {
                        savePath = openFileDialog.FileName;
                        TopNodes = XmlSerialiser.DeserialiseFromXML<ObservableCollection<Instruction>>(savePath);
                    }
                    else
                    {
                        MessageBox.Show("File Type Needed: .xml or .rcp", "Unacceptable File Type");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}

