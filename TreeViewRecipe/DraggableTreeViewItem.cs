﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ScriptingDevelopment.TreeViewRecipe
{
    public class DraggableTreeViewItem : TreeViewItem
    {
        private int _index;
        public int Index
        {
            get { return _index; }
            set { _index = value; }
        }

        public DraggableTreeViewItem()
        {
            this.Drop += (sender, e) =>
            {
                if (e.Data.GetData("DragableTreeViewItem") != null)
                    ((DraggableTreeViewItem)e.Data.GetData("DragableTreeViewItem")).SetParent(sender);
            };
        }

        public void SetParent(object newParent)
        {
            this.Index = 0;

            //if (Parent is TreeView)
            //    ((TreeView)Parent).Items.Remove(this);
            if (Parent is DraggableTreeViewItem)
                ((DraggableTreeViewItem)Parent).Items.Remove(this);

            //if (newParent is TreeView)
            //    ((TreeView)newParent).Items.Add(this);
            if (newParent is DraggableTreeViewItem)
                ((DraggableTreeViewItem)newParent).Items.Add(this);
        }



        //protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
        //{
        //    if (e.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
        //        DragDrop.DoDragDrop(this.Parent, new DataObject("DragableTreeViewItem", this, true), DragDropEffects.Move);

        //    base.OnMouseMove(e);
        //}

        protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            DragDrop.DoDragDrop(this.Parent, new DataObject("DraggableTreeViewItem", this, true), DragDropEffects.Copy);
            base.OnMouseDown(e);
        }

    }
}
