﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace ScriptingDevelopment.TreeViewRecipe
{
    public class DraggableTreeView : TreeView
    {
        public void DraggableTreeView_OnDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetData("DraggableTreeViewItem") != null)
            {
                var item = e.Data.GetData("DraggableTreeViewItem") as DraggableTreeViewItem;
                DraggableTreeViewItem newItem = new DraggableTreeViewItem();
                newItem.Header = item.Header;
                foreach (var child in item.Items)
                    newItem.Items.Add(child);
            }
        }

    }
    
}
