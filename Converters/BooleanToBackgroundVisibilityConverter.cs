﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace ScriptingDevelopment.Converters
{
    public class BooleanToBackgroundVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color color = Color.FromArgb((byte)40, (byte)33, (byte)99, (byte)33);
            if ((bool)value)
                return new SolidColorBrush((Color)ColorConverter.ConvertFromString(color.ToString()));
                //return Brushes.ForestGreen;
            else
                return Brushes.Transparent;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
