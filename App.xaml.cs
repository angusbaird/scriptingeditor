﻿using ScriptingDevelopment.Main;
using ScriptingDevelopment.TreeViewRecipe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ScriptingDevelopment
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            MainViewModel mainViewModel = new MainViewModel();
            MainView mainView = new MainView(mainViewModel);
            mainView.Show();

        }

    }
}
