﻿using ScriptingDevelopment.TreeViewRecipe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScriptingDevelopment.Main
{
    public class MainViewModel
    {
        public TreeViewRecipeUtility TreeViewRecipeUtility { get; set; }

        public MainViewModel()
        {
            TreeViewRecipeUtility = new TreeViewRecipeUtility();
        }
    
    }
}
