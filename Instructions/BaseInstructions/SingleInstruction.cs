﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ScriptingDevelopment.Instructions.BaseInstructions
{
    [Serializable]
    [XmlInclude(typeof(DelayInstruction))]
    public abstract class SingleInstruction : Instruction
    {
    }
}
