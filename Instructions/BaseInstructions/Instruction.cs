﻿using OptekMain.GenericClasses;
using ScriptingDevelopment.TreeViewRecipe;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ScriptingDevelopment.Instructions.BaseInstructions
{
    [XmlRoot(Namespace = "ScriptingDevelopment.Instructions")]
    [XmlInclude(typeof(BlockInstruction))]
    [XmlInclude(typeof(SingleInstruction))]
    public abstract class Instruction : ViewModel
    {
        public abstract string Name { get; }
        public int ID { get; set; } = DateTime.Now.TimeOfDay.Milliseconds; // temp, just to represent different instructions

        bool _isExecuting;
        [XmlIgnore]
        public bool IsExecuting
        {
            get
            {
                return _isExecuting;
            }
            set
            {
                _isExecuting = value;
                NotifyPropertyChanged();
            }
        }

        bool _isCommentedOut;

        public bool IsCommentedOut
        {
            get
            {
                return _isCommentedOut;
            }
            set
            {
                _isCommentedOut = value;
                NotifyPropertyChanged();
            }
        }


        protected abstract Func<CancellationToken, Task<bool>> Function { get; }

        protected abstract Func<CancellationToken, Task<bool>> VirtualFunction { get; }

        public virtual async Task<bool> Execute(CancellationToken Ct, bool Virtual = false) // maybe override this for block instuctions
        {
            try
            {
                if (IsCommentedOut)
                    return true;

                IsExecuting = true;
                if (!Virtual)
                {
                    return await Task.Run(() => Function(Ct));
                }
                else
                {
                    return await Task.Run(() => VirtualFunction(Ct));
                }


            }
            finally
            {
                IsExecuting = false;
            }
        }

        public override string ToString() // For {Binding}. used to be {Binding Name}
        {
            return string.Format("ID: {0}. Name: {1}", ID, Name);
        }

        bool IsVariable(string S)
        {
            return S[0] == '$';
        }

        bool IsEmpty(string S)
        {
            return S == null || S.Length == 0;
        }

    }
}

