﻿using ScriptingDevelopment.TreeViewRecipe;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ScriptingDevelopment.Instructions.BaseInstructions
{
    [Serializable]
    [XmlInclude(typeof(ForInstruction))]
    [XmlInclude(typeof(MainMethodBlock))]
    public abstract class BlockInstruction : Instruction
    {
        protected override Func<CancellationToken, Task<bool>> VirtualFunction => Function;

        public bool HasChildren => Children.Count > 0;

        public ObservableCollection<Instruction> Children { get; set; } = new ObservableCollection<Instruction>();
    }
}
