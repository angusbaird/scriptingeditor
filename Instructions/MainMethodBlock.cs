﻿using ScriptingDevelopment.Instructions.BaseInstructions;
using ScriptingDevelopment.TreeViewRecipe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ScriptingDevelopment.Instructions
{
    [Serializable]
    public class MainMethodBlock : BlockInstruction
    {
        public override string Name => "Main";


        public MainMethodBlock()
        { }


        protected override Func<CancellationToken, Task<bool>> Function
        {
            get
            {
                return async (Ct) =>
                {
                    return await Task.Run(() => true);
                };
            }
        }

        protected override Func<CancellationToken, Task<bool>> VirtualFunction
        {
            get
            {
                return async (Ct) =>
                {
                    return await Task.Run(() => true);
                };
            }
        }
    }
}
