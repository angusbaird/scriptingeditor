﻿using ScriptingDevelopment.Instructions.BaseInstructions;
using ScriptingDevelopment.TreeViewRecipe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ScriptingDevelopment.Instructions
{
    [Serializable]
    public class DelayInstruction : SingleInstruction
    {
        public override string Name => "Delay";

        private int _value;
        public int Value
        {
            get { return _value; }
            set 
            { 
                _value = value;
                NotifyPropertyChanged();
            }
        }





        public DelayInstruction()
        { }


        protected override Func<CancellationToken, Task<bool>> Function
        {
            get
            {
                return async (Ct) =>
                {
                    await Task.Delay(Value, Ct);
                    return true;
                };
            }
        }

        protected override Func<CancellationToken, Task<bool>> VirtualFunction
        {
            get
            {
                return async (Ct) =>
                {
                    MessageBox.Show("Delay for " + Value + "ms");
                    return true;
                };
            }
        }
    }
}
