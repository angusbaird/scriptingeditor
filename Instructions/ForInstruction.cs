﻿using ScriptingDevelopment.Instructions.BaseInstructions;
using ScriptingDevelopment.TreeViewRecipe;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ScriptingDevelopment.Instructions
{
    [Serializable]
    public class ForInstruction : BlockInstruction
    {
        public override string Name => "For";

        private int _iterations; //total iterations
        public int Iterations
        {
            get { return _iterations; }
            set
            {
                _iterations = value;
                DisplayedIterations = value - 1;
                NotifyPropertyChanged();
                NotifyPropertyChanged(nameof(DisplayedIterations));
            }
        }

        private int _displayedIterations; // For display only
        [XmlIgnore]
        public int DisplayedIterations
        {
            get { return _displayedIterations; }
            set
            {
                _displayedIterations = value;
                NotifyPropertyChanged();
            }
        }


        private int _currentIteration;
        [XmlIgnore]
        public int CurrentIteration
        {
            get { return _currentIteration; }
            set
            {
                _currentIteration = value;
                NotifyPropertyChanged();
            }
        }


        public ForInstruction()
        { }

        protected override Func<CancellationToken, Task<bool>> Function
        {
            get
            {
                return async (ct) =>
                {
                    return true;
                };
            }
        }

     
    }
}
